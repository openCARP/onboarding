IMAGE := $(or $(IMAGE), opencarp-jupyter:latest)
DOCKER_BASE := $(or $(DOCKER_BASE), ubuntu:22.04)

USER_ARGS := $(or $(USER_ARGS),--user $(shell id -u):$(shell id -g))
DOCKER_INSTANCE_NAME = opencarp-jupyter
DOCKER_RUN_FLAGS := $(or $(DOCKER_RUN_FLAGS), --rm --name $(DOCKER_INSTANCE_NAME) --network host)

# setup a directory to connect to the container
DOCKER_COMMAND := $(or $(DOCKER_COMMAND), python3 -m jupyterlab --allow-root --ip 0.0.0.0 --NotebookApp.token='')

help:
	@echo '--------------------------------------------------------------------------------'
	@echo '         Please use the make targets (with optional VARIABLES)'
	@echo '--------------------------------------------------------------------------------'
	@echo 'NOTE: specific env variables might affect functionality -- check the recipes'
	@echo '--------------------------------------------------------------------------------'
	@echo 'build image:  DOCKER_EXTRA_FLAGS=""                make dockerbuild'
	@echo 'run in image: DOCKER_COMMAND="" IMAGE=""           make dockerrun'
	@echo 'connect to a running container instance:           make dockerconnect'
	@echo 'this help:                                         make'
	@echo '--------------------------------------------------------------------------------'

dockerbuild: Dockerfile
	# DOCKER_BUILDKIT=0 is for compatiblity with mybinder.org
	DOCKER_BUILDKIT=0 docker build \
		$(DOCKER_EXTRA_FLAGS) \
		--build-arg "DOCKER_BASE=$(DOCKER_BASE)" \
		--build-arg "NB_USER=$(shell id -un)" \
		--build-arg "NB_UID=$(shell id -u)" \
		--tag $(IMAGE) \
		-f $< .

.PHONY: dockerrun
dockerrun:
	@echo "Running in docker container:"
	@echo "Note: re-run 'make dockerbuild' if you changed anything."
	docker run --interactive --tty $(USER_ARGS) \
		$(DOCKER_RUN_FLAGS) \
		--volume $(shell pwd):/openCARP/external/onboarding \
		$(IMAGE) $(DOCKER_COMMAND)

.PHONY: dockerconnect
dockerconnect:
	@echo "Connecting to docker container:"
	docker exec --interactive --tty $(USER_ARGS) \
		$(DOCKER_INSTANCE_NAME) \
		bash
