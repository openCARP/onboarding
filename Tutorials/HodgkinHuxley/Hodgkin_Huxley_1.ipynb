{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "title: Hodgkin-Huxley Tutorial\n",
    "subtitle: Rate constants of the gating variables\n",
    "license: CC-BY-4.0\n",
    "subject: Tutorial\n",
    "venue: openCARP onboarding\n",
    "authors:\n",
    "  - name: Gunnar Seemann\n",
    "    email: gunnar.seemann@kit.edu\n",
    "    corresponding: true\n",
    "    orcid: 0000-0001-7111-7992\n",
    "  - name: Eike M. Wülfers\n",
    "    orcid: 0000-0002-8292-6514\n",
    "  - name: Robin Moss\n",
    "    orcid: 0000-0001-8738-0461\n",
    "  - name: Viviane Timmermann\t\n",
    "    orcid: 0000-0003-2044-8307\n",
    "date: 2024/02/05\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    ":::{admonition} Don't know how to start?\n",
    ":class: note\n",
    "You should know by now how to execute a jupyter cell and how to use our helping system. If this is new to you, please first do the [basic single cell toturial](../single_cell/01_basic_bench/tutorial.ipynb).\n",
    ":::\n",
    "\n",
    "Alan Hodgkin and Andrew Huxley described the model in 1952 to explain the ionic mechanisms underlying the action potential in the squid giant axon. They received in 1963 the Nobel Prize for this work.\n",
    "\n",
    "The Hodgkin–Huxley model is a mathematical model describing action potentials in neurons. It is a set of nonlinear differential equations that approximates the equivalent of an eletrical circuit of excitable cells. The cell membrane (lipid bilayer) is represented as a capacitance ($C_m$). Voltage-gated ion channels are represented by electrical conductances ($g_X$, where $X$ is the specific ion type) that are voltage and time dependent. Remaining current components are described as leak currents represented by a linear conductance ($g_L$). The electrochemical gradients driving the flow of ions are represented by voltage sources ($E_X$). These voltages are depending on the ratio of the intra- and extracellular concentrations of the ion type $X$. The membrane voltage is denoted by $V_m$. The following diagram shows the eletrical equivalent circuit.\n",
    "\n",
    "```{image} circuit.png\n",
    ":width: 400px\n",
    "```\n",
    "\n",
    "The ion current via the cell membrane is described by the capacitive equation:\n",
    "\n",
    "$I_C=C_m \\frac{dV_m}{dt}$\n",
    "\n",
    "The current through a given ion channel of type $X$ is the product of it's conductance $g_X$ and the effective driving voltage for the specific ion type being the difference between $V_m$ and $E_X$:\n",
    "\n",
    "$I_X=g_X (V_m -E_X)$\n",
    "\n",
    "Thus the total membrane current $I_{mem}$ through all ion channels and the membrane itself is:\n",
    "\n",
    "$I_{mem} = C_m \\frac{dV_m}{dt} + g_{Na} (V_m -E_{Na}) + g_{K} (V_m -E_{K}) +g_{L} (V_m -E_{L})$\n",
    "\n",
    "In voltage-gated ion channels, the channel conductance is a function of both time and voltage ($g_X(V_m, t))$. The gating kinetics describes the protein configuration that is important for its ion conductance. One single ion channel protein can be either  opened or closed. As many of similar proteins of the same type exists in one cell, some can be open and some closed thus leading to a probability that proteins of the same type are in the open position. If the fraction of proteins in the open state is $\\gamma$, the fraction of closed proteins is ($1 - \\gamma$). $\\gamma$ is also called gate. Considering the rate constants $\\alpha_\\gamma$ and $\\beta_\\gamma$ describing the trasitions between open and closed state, this can be illustrated as a 2-state Markov process:\n",
    "```{image} kinetics.png\n",
    ":width: 150px\n",
    "```\n",
    "The change in $\\gamma$ over time is the difference between opening $\\alpha_\\gamma (1 - \\gamma)$ and closing $\\beta_\\gamma \\gamma$:\n",
    "\n",
    "$\\frac{d\\gamma}{dt}=\\alpha_\\gamma (1 - \\gamma) - \\beta_\\gamma \\gamma$\n",
    "\n",
    "Potassium channels are formed by four proteins of the same type responsible for activation. These gates is named $n$. The probability of the the channel to be open is the product of the open probability of its proteins or gates. Thus $g_{K}=\\bar g_{K} n^4$ with $\\bar g_{K}$ being the maximum conductance of the channel. For sodium channels, we have three activation gates $m$ and one inactivation gate $h$. This leads to $g_{Na}=\\bar g_{Na} m^3h$. Hodgkin and Huxley found the following equations fitting their experimental data the best:\n",
    "\n",
    "$\\alpha_n = 0.01 \\frac{ -(V+55)}{\\exp(\\frac{-(V+55)}{10}) - 1}\\ \n",
    "\\beta_n = 0.125 \\exp\\left(\\frac{-(V+65)}{80}\\right)\\\\ \n",
    "\\\\\n",
    "\\alpha_m = 0.1 \\frac{ -(V+40)}{\\exp\\left(\\frac{-(V+40)}{10}\\right) - 1}\\\n",
    "\\beta_m = 4 \\exp\\left(\\frac{-(V+65)}{18}\\right)\\\\ \n",
    "\\\\\n",
    "\\alpha_h = 0.07 \\exp\\left(\\frac{-(V+65)}{20}\\right)\\\n",
    "\\beta_h = \\frac{ 1}{\\exp\\left(\\frac{-(V+35)}{10}\\right) + 1}$\n",
    "\n",
    "This leads to the following voltage dependency of the rate constants: \n",
    "```{image} image1.png\n",
    ":width: 500px\n",
    "```\n",
    "\n",
    "## Learning Goals\n",
    "This first Hodgkin-Huxley tutorial should familiarize you with the rate constants of the model. You will learn to\n",
    "- define an array with the necessary membrane voltages\n",
    "- allocate memory for the rate constants\n",
    "- calculate limits for some rate constants\n",
    "- calculate the rate constants\n",
    "- visualize graphs with matplotlib\n",
    "\n",
    "\n",
    "## Task:\n",
    "Implement the equations in python as follows:\n",
    "\n",
    "- Calculate $\\alpha_n$,  $\\beta_n$,  $\\alpha_m$, $\\beta_m$,  $\\alpha_h$, $\\beta_h$ for voltages ranging from -100 mV to +40 mV (in steps of 1 mV)\n",
    "- Plot your results in a graph\n",
    "- Interpret the behavior of the curves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Your Implementation\n",
    "### Preamble\n",
    "This is the place to put import statements for the modules that you want to use in your code. For now, just execute this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt # This is for the plotting\n",
    "from math import exp\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Limits Calculation\n",
    "- For $\\alpha_n$, $\\alpha_m$ and $\\beta_h$ the denominators get zero for specific values of $V_m$. What are these values?\n",
    "- Calculate the limits for these values (you can use Python or WolframAlpha for example)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your limits calculation\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialization\n",
    "Initialize a list containing the transmembrane voltages steps you’re going to simulate. Create variables of the same size to hold the gating variables at each voltage step (memory allocation)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your implementation\n",
    "Vstep = ...\n",
    "\n",
    "alpha_n = ...\n",
    "beta_n  = ...\n",
    "\n",
    "alpha_m = ...\n",
    "beta_m  = ...\n",
    "\n",
    "alpha_h = ...\n",
    "beta_h  = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gating Variable Calculation\n",
    "Implement the calculation of the gating variables for all voltages under consideration. Consider your limits calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your implementation\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting of Results\n",
    "Just execute the following cell for the visualization of the results and compare it to the image in the Introduction section. Describe the behaviour of the curves. What is the main difference between the gating variables of the $n$, $m$ and $h$ gate?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# The following function creates a figure with three subplots. We can access the subplots by using\n",
    "# the returned list `axes`.\n",
    "fig, axes = plt.subplots(nrows=3, sharex=True, sharey=False, figsize=(8, 8))\n",
    "axes[0].plot(Vstep, alpha_n, label='$\\\\alpha_n$')\n",
    "axes[0].plot(Vstep, beta_n, label='$\\\\beta_n$')\n",
    "axes[0].legend(loc='upper left')\n",
    "axes[0].set_ylabel('Rate constant (1/ms)')\n",
    "\n",
    "lines = axes[1].plot(Vstep, alpha_m, Vstep, beta_m)\n",
    "axes[1].legend(lines, ['$\\\\alpha_m$', '$\\\\beta_m$'], loc='upper right')\n",
    "axes[1].set_ylabel('Rate constant (1/ms)')\n",
    "\n",
    "axes[2].plot(Vstep, alpha_h, Vstep, beta_h)\n",
    "axes[2].legend(['$\\\\alpha_h$', '$\\\\beta_h$'], loc='upper left')\n",
    "axes[2].set_xlabel('Transmembrane voltage (mV)')\n",
    "axes[2].set_ylabel('Rate constant (1/ms)')\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
