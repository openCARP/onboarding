{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7fd00f2c-690c-4a9d-af67-c9fde0bfc86d",
   "metadata": {
    "editable": false
   },
   "source": [
    "---\n",
    "title: Single Cell Tutorial\n",
    "subtitle: Basic functionality I\n",
    "license: CC-BY-4.0\n",
    "subject: Tutorial\n",
    "venue: openCARP onboarding\n",
    "authors:\n",
    "  - name: Gunnar Seemann\n",
    "    email: info@opencarp.org\n",
    "    corresponding: true\n",
    "    orcid: 0000-0001-7111-7992\n",
    "date: 2024/01/30\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f41d0ea5-26de-42fd-9048-432196792b19",
   "metadata": {
    "editable": true
   },
   "source": [
    "## Introduction\n",
    ":::{admonition} New to Jupyter Lab?\n",
    ":class: note\n",
    "If you don't know how to execute a jupyter cell and/or if you need a short introduction to jupyter, please watch the [introduction to jupyter](https://www.youtube.com/watch?v=A5YyoCKxEOU).\n",
    ":::\n",
    "🛠 Whenever you see this symbol, you have a task in the following cell. Cells can be executed be clicking into them and pressing ```shift+enter```. You can also select from the jupyter menu \"Run\" the entry \"Run Selected Cell\". 🛠 \n",
    "\n",
    ":::{admonition} Don't know how to proceed in a tutorial?\n",
    ":class: note\n",
    "If you don't know what to do, please also execute the cell without typing anything. You can get help interactively. You can do this in all cells of all tutorials where you need to add information by yourself. These lines always start with ```display_quiz```and you should NOT edit this line.\n",
    ":::\n",
    "\n",
    ":::{important}\n",
    "If you see pink background in the output of a cell, python is not used correctly. Maybe it would be a good idea to [learn more about python](https://swcarpentry.github.io/python-novice-inflammation/index.html).\n",
    ":::\n",
    "\n",
    "Within the openCARP ecosystem, the single cell program is called ```bench```. It can be used to replicate a wider range of single cell experimental protocols. Bench handles all time units in $ms$, all voltages in $mV$, and currents in $pA/pF$ (or $μA/cm^2$ considering the fixed membrane capacitance $C_m$ of $1pF/cm^2$). Further information regarding ```bench```, its functionality and options can be found in the [openCARP user manual](https://opencarp.org/documentation/user-manual) in sections 5 and 8.\n",
    "\n",
    "```bench``` is a command line program, so you would normally execute it in a [Unix shell](https://swcarpentry.github.io/shell-novice/), e.g. ```bash```. Here in jupyter lab, we are calling ```bench``` within a jupyter cell. Later on you learn how to use the python-based framework ```carputils``` to build complete experiments with ```bench```. The first four tutorials are designed so that you learn the command line options of ```bench```, the following two tutorials introduce the ```carputils``` functionality of ```bench``` so that you can develop python experiments. A set of single cell exeriments can be found on the [example webpage](https://opencarp.org/documentation/examples#electrophysiology-in-single-cell).\n",
    "\n",
    "## Learning Goals\n",
    "This first tutorial should familiarize you with the basic functionality of the ionic model program ```bench```. You will learn to\n",
    "- use jupyter\n",
    "- list all options of ```bench```\n",
    "- see which ionic models are implemented in openCARP incl. their metadata and parameters\n",
    "- run a simulation with the ten Tusscher and Panfilov ionic model\n",
    "- visualize graphs with matplotlib\n",
    "- change parameters in the model and evaluate their effects on the model\n",
    "\n",
    "OK, now enough reading, let's get started.\n",
    "\n",
    "## Importing python libraries\n",
    "🛠 Please execute the following cell in this notebook.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f6e2dc51-7869-4bfe-9bff-a09ba5214e0b",
   "metadata": {
    "editable": false
   },
   "outputs": [],
   "source": [
    "import ipywidgets as widgets\n",
    "import subprocess\n",
    "import pandas as pd\n",
    "from random import randint\n",
    "import matplotlib.pyplot as plt\n",
    "from jupyterquiz import display_quiz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cedf3a3d-5043-4c76-a5dd-23d908debe8b",
   "metadata": {},
   "source": [
    "So, you should have executed the cell with clicking into it and ```shift+enter```. The number 1 should be in the brackets left of the cell. This cell just loads libraries and tools that we need later on.\n",
    "## Options of ```bench```\n",
    "\n",
    "Now, let's have a look at your first ```bench``` execution to see what command line options it offers. Please always leave the first entry of the cells unchanged as they will give you hints if you don't get what you want or if you don't know what to do.\n",
    "\n",
    "🛠 OK, please execute ```bench``` with showing the command line options. Therefore, you need to add something in the line below ```display_quiz(...)```. If you don't know at all what to do, go back to the [introduction](#introduction)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab55baea-1268-4622-aa7e-756be2584f40",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q01.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "# Add your input below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e62456ba-7ecb-4925-a52f-f8fb9428ee2a",
   "metadata": {},
   "source": [
    ":::{admonition} Do you see pink background and yellow marked ```display_quiz(...)```?\n",
    ":class: important dropdown\n",
    "The python libraries are unknown at this point. You likely did not execute the first cell. Please go into secion [Importing python libraries](#importing-python-libraries), click into the cell starting with ```import ipywidgets as widgets```, and ```shift+enter```.\n",
    ":::\n",
    "\n",
    "OK, now that you see all command line options, let's go through the basic ones. More advanced options will follow in subsequent tutorials.\n",
    ":::{admonition} Do you want more information?\n",
    ":class: note dropdown\n",
    "If you need more detailed information about the commands for ```bench```, you can run bench with ```--detailed-help``` instead of ```--help```\n",
    "\n",
    ":::\n",
    "\n",
    "## List of available single cell models\n",
    "Find the command line option of how to list all available single cell models in the above output.\n",
    "\n",
    "🛠 Call ```bench``` again now with the respective command to list all ionic models. All command are listed in the [previous cell](#options-of-bench)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08246b4f-b27c-44e5-999a-5a201e3f7034",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q02.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8daadd58-30b2-4fdb-ac16-3bc41f407716",
   "metadata": {},
   "source": [
    "So, you should see a list of ionic models available in ```bench``` (and later on in the tissue simulator ```openCARP```) followed by available plugins. Plugins could be e.g., extensions of ionic models like additional ion channels or stress models to calculate mechanical tension. We will learn more about plugins in following tutorials.\n",
    "## Ionic model metadata and parameters\n",
    "Next, we learn more about the chosen ionic models.\n",
    "\n",
    "🛠 Please select the _ten Tusscher and Panfilov_ ionic model in the command line and add the option to see the ionic model information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37f888f6-af60-41a7-866f-9ca5c8aa19c6",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q03.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47d30e11-4d88-4310-ab1c-2736bc0e0fad",
   "metadata": {},
   "source": [
    "In the \"Metadata\" section you find more information of the publication of the ionic model. The \"Parameter\" section lists the parameters of the model and the section \"State Variables\" the variable for which a partial differential equation needs to be solved (see section 3 of the [openCARP user manual](https://opencarp.org/documentation/user-manual), our basic [video tutorial](https://opencarp.org/documentation/video-tutorials#introduction-to-ionic-models) or at literature, e.g., [this](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6561094/)). \n",
    "## Standard execution\n",
    "The output of ```bench``` is a table with the first column beeing the time and the following columns beeing each state variable. At the last column, the transmembrane voltage $V_m$ is added. The rows of the table are the values of each of theses values with progressing time.\n",
    "\n",
    "🛠 Let's run ```bench``` now with the _ten Tusscher and Panfilov_ ionic model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7ecfad5-7b09-4bbb-a714-761a043b5e52",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q04.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "487778eb-03ce-4612-8d93-ffe140f65694",
   "metadata": {},
   "source": [
    "So, when you execute bench just with ```--imp tenTusscherPanfilov```, you will get an output showing you at the beginning what version of openCARP is used, followed by a table of time, $V_m$ and the sum of ionic currents $I_{ion}$. At the end of the output, you see some performance measures. When you don't specify to which file the optput of the simulation should be written, it will be in \"Trace_0.dat\". You should see this file on the left side in the navigation by clicking on ![folder](help/icons8-folder-24.png). Please navigate into the folder `/single_cell/01_basic_bench` and double click the file \"Trace_0.dat\". Here you see the mentioned data table. If you need the information which column is what, you can click in the left navigation on \"tenTusscherPanfilov_trace_header.txt\".\n",
    "## Visualization of results\n",
    "We are going to use [matplotlib](https://matplotlib.org) for the ionic model visualisations. [This](https://youtu.be/D4VlmL3G4_o) is a basic video introduction to matplotlib. We have prepared the plotting in the following. \n",
    "\n",
    "🛠 Please just execute the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6699889a-fbe0-4704-840f-47a696a1b561",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = pd.read_csv(\"Trace_0.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.plot(data[0],data[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bef73c9f-5492-4370-806a-1bc8555e021c",
   "metadata": {},
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/image1.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "You should see the temporal evolution of the variable 2 of the ten Tusscher model. Change the 2 in ```data[2]``` to the $V_m$ related number (again look into \"tenTusscherPanfilov_trace_header.txt\" to identify the column number of $V_m$ (it is named just \"V\").\n",
    ":::{admonition} This is the result that you should now see\n",
    ":class: note dropdown\n",
    "```{image} help/image2.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "We are using the library pandas to read Trace_0.dat via the function read_csv. Tab (\"\\t\") is used as separator.\n",
    "## Changing parameters\n",
    "The last thing of this first tutorial is to change a parameter of the model and visualize the differences compared to the standard model. You can modify parameters of the model with the option ```--imp-par```. In the [first example](https://opencarp.org/documentation/examples/01_ep_single_cell/01_basic_bench) on the openCARP webpage, the usage of ```--imp-par``` is explained. For now, please double the conductivity of the slow delayed recifyer potassium current $G_{Ks}$. If you are not sure, how the variable is named, look at the [model parameters](#Ionic-model-metadata-and-parameters).\n",
    ":::{important}\n",
    "Before you run the second command, please add ```--trace-no=1``` to the command line so that a new file with the name \"Trace_1.dat\" will be generarted.\n",
    ":::\n",
    "🛠 Please now run the second simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a94112c-8837-47d1-b22d-4c326e4e8e14",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q05.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "315577e3-f050-4692-a81e-f6195534f977",
   "metadata": {},
   "source": [
    "🛠 Now, just execute the next cell for visualization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "018c3575-c338-4c2d-9aaa-0d44c03f36fd",
   "metadata": {},
   "outputs": [],
   "source": [
    "data2 = pd.read_csv(\"Trace_1.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.plot(data[0],data[16], label='first simulation')\n",
    "plt.plot(data[0],data2[16], label='second simulation')\n",
    "plt.legend(loc='upper right')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e84ccb80-29bd-4193-9e85-3540d65f28d3",
   "metadata": {},
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/image3.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "If you want to play around with the parameters, go back two jupyter cells, change parameters, execute the cell, and execute the second visualization cell again."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbc46e00-558f-4356-ba75-f7fe2601e321",
   "metadata": {},
   "source": [
    "::::{grid} 1 2 2 3\n",
    ":gutter: 1 1 1 2\n",
    "\n",
    ":::{grid-item-card}\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    ":header: 🏡 Tutorial Overview\n",
    ":link: ../../index.ipynb\n",
    ":link-type: ref\n",
    "Go back to the overview page of the tutorials\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    ":header: Basic Single Cell Tutorial II ➡️\n",
    ":link: ../02_basic_bench/tutorial.ipynb\n",
    ":link-type: ref\n",
    "Introducing stimulation, duration, and integration aspects\n",
    ":::\n",
    "\n",
    "::::\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
