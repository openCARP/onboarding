FROM docker.opencarp.org/opencarp/opencarp:docker-ubuntu-22-04 as opencarp-jupyter

COPY requirements.txt .
# RUN --mount=type=cache,target=/root/.cache/pip \
RUN \
  python3 -m pip install -r requirements.txt

WORKDIR /openCARP/experiments
COPY . ./onboarding
WORKDIR /openCARP/experiments/onboarding/Tutorials

ARG NB_USER=jovyan
ARG NB_UID=1000
RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

RUN chown -R ${NB_UID}:${NB_UID} .
RUN jupyter labextension disable "@jupyterlab/apputils-extension:announcements"

ENTRYPOINT []
