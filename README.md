# Onboarding to openCARP

[![Binder](https://mybinder.org/badge_logo.svg)][mybinder]

The onboarding is separated into a knowledge selfevaluation planned to be only web page based on grav and a tutorial section to be run in jupyter lab.

For a quick start use [the binder link][mybinder] which will build and start a
 virtual machine from a container on a remote server. You will get an access
 through the web interface. All the required dependencies are already installed
 there. So you can just go ahead and complete the tutorials.

If you prefer to run the tutorials on your own machine, follow the steps below.

[mybinder]: https://mybinder.org/v2/git/https%3A%2F%2Fgit.opencarp.org%2FopenCARP%2Fonboarding.git/stable?labpath=index.ipynb

## Prerequisites for running the tutorials on your own computer

The only prerequisites is a functioning docker setup on your computer. You can either use [Docker Desktop](https://www.docker.com/products/docker-desktop/) (Linux/Windows/Mac) or alternatively command line installations:

On **Debian based GNU/Linux** distributions installed by:
```
apt-get update && apt install docker
```
After that you might need [to add yourself to the docker group with elevated
permissions](https://docs.docker.com/engine/install/linux-postinstall/).

For **macOS** this can be done running:
```
brew install colima docker
colima start --cpu 8 --memory 8
```

Then you can check if everything is working by spawning a test container:
```
docker run hello-wold
```

## Getting started on your own computer

Clone the repository to the location where you would like to have it and change to its root directory
```
git clone https://git.opencarp.org/openCARP/onboarding.git
cd onboarding
docker-compose up
```

Now you can see the onboarding with clicking on http://127.0.0.1:8888/lab/tree/index.ipynb 

There is one folder for a Hodgkin-Huxley tutorial, one for single cell tutorials and one for tissue simulations. In each folders are subfolders with the tutorials. Each tutorial's name is `tutorial.ipynb`.

To stop the session, go into the terminal and press `Ctrl-c`. 

If you want to submit your changes, you need download the changed file from your jupyterlab and copy it to the corresponding location of your local onboarding directory located in `onboarding/Tutorials/...`

Afterwards, follow the usual steps for submitting changes to a git repository:
```
git add <file> ("git status" also shows all changes)
git commit -m "your message"
git push
```

## Make Setup (Deprecated)

NOTE: `make` setup has been deprecated in favour of native `docker-compose`.

Build the docker container using `make`, just running without any arguments
shows the help and you can always to use `make --dry` switch to see the commands
to be executed without actually running them. However, the default options for
the targets should be reasonable, so for a quick start just run:
```
make dockerbuild
make dockerrun
```
which will build and run the container
`jupyter-opencarp`. It will also connect the repository inside the container, so
any changes you make in the files, will be saved in the repository.

If you have already done the previous step, you should update the onboarding repository and rerun the making of the docker
```
cd onboarding
git pull
make dockerbuild
make dockerrun
```
